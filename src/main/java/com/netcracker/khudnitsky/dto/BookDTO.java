package com.netcracker.khudnitsky.dto;

import java.io.Serializable;
import java.util.Set;

public class BookDTO implements Serializable {
  private String name;
  private Set<String> publishers;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Set<String> getPublishers() {
    return publishers;
  }

  public void setPublishers(Set<String> publishers) {
    this.publishers = publishers;
  }
}
