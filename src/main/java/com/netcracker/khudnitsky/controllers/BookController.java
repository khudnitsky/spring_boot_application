package com.netcracker.khudnitsky.controllers;

import com.netcracker.khudnitsky.dto.BookDTO;
import com.netcracker.khudnitsky.services.BookService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api
@RestController
@RequestMapping("/books")
public class BookController {

  private final BookService service;

  public BookController(BookService service) {
    this.service = service;
  }

  @ApiOperation(value = "Gets all books", nickname = "BookController.getAllBooks")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Books")})
  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Iterable<BookDTO>> getAllBooks() {
    Iterable<BookDTO> books = service.findAllBooks();
    return new ResponseEntity<>(books, HttpStatus.OK);
  }

  @ApiOperation(value = "Gets specific book", nickname = "BookController.getBook")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Book")})
  @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<BookDTO> getBook(@PathVariable("id") String id) {
    BookDTO book = service.getBook(id);

    if(book == null){
      return new ResponseEntity<>(book, HttpStatus.NOT_FOUND);
    }
    return new ResponseEntity<>(book, HttpStatus.OK);
  }

  @ApiOperation(value = "Creates book", nickname = "BookController.createBook")
  @ApiResponses(value = {@ApiResponse(code = 201, message = "Book is created")})
  @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<String> createBook(@RequestBody BookDTO book) {
    try {
      String id = service.createBook(book);
      return new ResponseEntity<>(id, HttpStatus.CREATED);
    } catch (Exception e) {
      return new ResponseEntity<>("Error in creation book", HttpStatus.BAD_REQUEST);
    }
  }

  @ApiOperation(value = "Deletes book", nickname = "BookController.deleteBook")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Book is deleted")})
  @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Void> deleteBook(@PathVariable("id") String id) {
    try {
      service.deleteBook(id);
      return new ResponseEntity<>(HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
  }
}
