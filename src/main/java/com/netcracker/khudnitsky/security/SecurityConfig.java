package com.netcracker.khudnitsky.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.AuthenticationEntryPoint;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

  @Autowired
  private AuthenticationEntryPoint authEntryPoint;

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http
        .csrf().disable()
        .authorizeRequests()
        .mvcMatchers(HttpMethod.GET, "/books/**").authenticated()
        .mvcMatchers(HttpMethod.POST, "/books/**").permitAll()
        .mvcMatchers(HttpMethod.PUT, "/books/**").permitAll()
        .mvcMatchers(HttpMethod.DELETE,"/books/**").hasRole("ADMIN")
        .and().httpBasic().authenticationEntryPoint(authEntryPoint);
  }

  @Autowired
  public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
    auth.inMemoryAuthentication().withUser("user").password("user").roles("USER");
    auth.inMemoryAuthentication().withUser("admin").password("admin").roles("ADMIN");
  }
}