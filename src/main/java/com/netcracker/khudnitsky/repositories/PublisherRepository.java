package com.netcracker.khudnitsky.repositories;

import com.netcracker.khudnitsky.model.Publisher;
import org.springframework.data.repository.CrudRepository;

public interface PublisherRepository extends CrudRepository<Publisher, Long> {

  Publisher getByName(String name);
}
