package com.netcracker.khudnitsky.repositories;

import com.netcracker.khudnitsky.model.Book;
import org.springframework.data.repository.CrudRepository;

public interface BookRepository extends CrudRepository<Book, Integer> {
}
