package com.netcracker.khudnitsky.services;

import com.netcracker.khudnitsky.dto.BookDTO;
import com.netcracker.khudnitsky.model.Book;
import com.netcracker.khudnitsky.model.Publisher;
import com.netcracker.khudnitsky.repositories.BookRepository;
import com.netcracker.khudnitsky.repositories.PublisherRepository;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;

@Service
public class BookService {

  private final BookRepository bookRepository;
  private final PublisherRepository publisherRepository;


  public BookService(BookRepository bookRepository,
      PublisherRepository publisherRepository) {
    this.bookRepository = bookRepository;
    this.publisherRepository = publisherRepository;
  }

  public List<BookDTO> findAllBooks(){
    Iterable<Book> books = bookRepository.findAll();

    List<BookDTO> dtos = new ArrayList<>();
    for (Book book : books){
      dtos.add(convert(book));
    }

    return dtos;
  }

  public BookDTO getBook(String id) {
    Book book = bookRepository.findOne(Integer.valueOf(id));
    return book != null ? convert(book) : null;
  }

  public String createBook(BookDTO book) {
    Book savedBook = bookRepository.save(convert(book));
    return String.valueOf(savedBook.getId());
  }

  private BookDTO convert (Book book){
    BookDTO dto = new BookDTO();

    dto.setName(book.getName());
    dto.setPublishers(book.getPublishers().stream().map(Publisher::getName).collect(Collectors.toSet()));

    return dto;
  }

  private Book convert (BookDTO dto){
    Book book = new Book();

    book.setName(dto.getName());

    Set<Publisher> publishers = new HashSet<>();

    for(String name : dto.getPublishers()){
      Publisher publisher = publisherRepository.getByName(name);
      publishers.add(publisher);
    }

    book.setPublishers(publishers);

    return book;
  }

  public void deleteBook(String id) {
    bookRepository.delete(Integer.valueOf(id));
  }
}
